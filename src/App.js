import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const web3 = window.web3;

function App() {
	return (
		<Router>
			<Route exact path="/" component={Index} />
			<Route path="/block/:id" component={Block} />
			<Route path="/transaction/:id" component={Transaction} />
		</Router>
	);
}

// views
function Index() {
	const [blocks, setBlocks] = useState([]);
	const [error, setError] = useState(null);

	useEffect(() => {
		fetchLastBlocks()
			.then(setBlocks)
			.catch(setError);
	}, []);

	return (
	<div>
		<p>{error ? `error -> ${error.message}` : ''} </p>
		<h2>Last blocks</h2>
		<ul>
			{blocks.map(block => <li key={block}>
				<Link to={"/Block/" + block}>block {block}</Link>
			</li>)}
		</ul>
	</div>);
}


function Block({ match }) {
	const [block, setBlock] = useState(null);
	const [error, setError] = useState(null);

	useEffect(() => {
		fetchBlock(match.params.id)
			.then(filterBlock)
			.then(setBlock)
			.catch(setError);
	}, [match.params.id]);

	return (
	<div>
		<p>{error ? `error -> ${error.message}` : ''} </p>
		<Link to={"/"}>Main</Link>
		{block ?
			<div>
				<h2>Block {block.number}</h2>
				<p>gasLimit : {block.gasLimit}</p>
				<p>gasUsed: {block.gasUsed}</p>
				<p>hash: {block.hash}</p>
				<p>miner: {block.miner}</p>
				<p>Transactions: </p>
				<ul>
					{block.transactions.map(transaction =>
						<li key={transaction}>
							<Link to={"/Transaction/" + transaction.toString()}>
								{transaction}
							</Link>
						</li>)}
				</ul>
			</div> :
			<p>...loading</p>}
	</div>);
}


function Transaction({ match }) {
	const [transaction, setTransaction] = useState(null);
	const [error, setError] = useState(null);

	useEffect(() => {
		fetchTransaction(match.params.id)
			.then(setTransaction)
			.catch(setError);
	}, [match.params.id]);

	return (
	<div>
		<p>{error ? `error -> ${error.message}` : ''} </p>
		{transaction ?
			<div>
				<h2>Transaction</h2>
				<p>blockHash : {transaction.blockHash}</p>
				<p>blockNumber :  <Link to={"/Block/" + transaction.blockNumber}>{transaction.blockNumber}</Link> </p>
				<p>from : {transaction.from}</p>
				<p>gas : {transaction.gas}</p>
				<p>gasPrice : {transaction.gasPrice.toNumber()} wei </p>
				<p>hash : {transaction.hash}</p>
				<p>to : {transaction.to}</p>
				<p>value : {transaction.value.toNumber() / 1000000000000000000} ether</p>
			</div> :
			<p>...loading</p>}
	</div>);
}

// effects
const fetchLastBlocks = () => {
	return new Promise((resolve, reject) => {
		web3.eth.getBlockNumber((error, lastBlock) => {
			if (error) {
				reject(error);
			}
			let result = [1, 2, 3, 4, 5, 6, 7, 8, 9]
				.reduce((acc, item) => acc.concat(lastBlock - item), [lastBlock]);
			return resolve(result);
		})
	})
}

const fetchBlock = id => {
	return new Promise((resolve, reject) => {
		web3.eth.getBlock(id, (error, block) => {
			if (error) {
				reject(error);
			}
			return resolve(block);
		})
	})
}

const filterBlock = block => {
	return new Promise((resolve, reject) => {
		Promise.all(block.transactions.map(fetchTransaction))
			.then(transactions => {
				let _block = {
					...block
					, transactions: transactions
						.filter(t => t.value.toNumber() > 0)
						.map(t => t.hash)
				};
				return resolve(_block);
			})
	})
}

const fetchTransaction = id => {
	return new Promise((resolve, reject) => {
		web3.eth.getTransaction(id, (error, transaction) => {
			if (error) {
				reject(error);
			}
			return resolve(transaction);
		})
	})
}

export default App;